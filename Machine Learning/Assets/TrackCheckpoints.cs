﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackCheckpoints : MonoBehaviour 
{

    public Transform CheckPoints;

    private List<CheckpointSingle> checkpointSingleList;
    private int nextCheckpointSingleIndex;

    public delegate void OnPlayerCorrectCheckpointDelegate();
    public event OnPlayerCorrectCheckpointDelegate OnPlayerCorrectCheckpoint;

    public delegate void OnPlayerWrongCheckpointDelegate();
    public event OnPlayerWrongCheckpointDelegate OnPlayerWrongCheckpoint;

    private void Awake() {
        Transform checkpointsTransform = CheckPoints;

        checkpointSingleList = new List<CheckpointSingle>();
        foreach (Transform checkpointSingleTransform in checkpointsTransform) {
            CheckpointSingle checkpointSingle = checkpointSingleTransform.GetComponent<CheckpointSingle>();

            checkpointSingle.SetTrackCheckpoints(this);

            checkpointSingleList.Add(checkpointSingle);
        }
        nextCheckpointSingleIndex = 0;
    }

    private void Start()
    {
        checkpointSingleList[0].Show();
    }

    public void CarThroughCheckpoint(CheckpointSingle checkpointSingle) {
        
        if (checkpointSingleList.IndexOf(checkpointSingle) == nextCheckpointSingleIndex) {
            Debug.Log("Correct");
            
            CheckpointSingle correctCheckpointSingle = checkpointSingleList[nextCheckpointSingleIndex];
            correctCheckpointSingle.Hide();
            correctCheckpointSingle.disableCollider();

            nextCheckpointSingleIndex = (nextCheckpointSingleIndex + 1) % checkpointSingleList.Count;
            checkpointSingleList[nextCheckpointSingleIndex].Show();

            OnPlayerCorrectCheckpoint?.Invoke();

        } else {
            Debug.Log("Wrong");
            OnPlayerWrongCheckpoint?.Invoke();
        }
    }

    public void ResetCheckpoint()
    {
        nextCheckpointSingleIndex = 0;
        foreach (var check in checkpointSingleList)
        {
            check.Hide();
            check.enableCollider();
        }
        checkpointSingleList[0].Show();
    }

    public CheckpointSingle GetNextCheckpoint()
    {
        return checkpointSingleList[nextCheckpointSingleIndex];
    }

}
