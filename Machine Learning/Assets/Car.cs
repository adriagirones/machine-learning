﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;

public class Car : Agent
{
    [SerializeField]
    TrackCheckpoints trackCheckpoints;

    [SerializeField]
    Transform initialPosition;

    private Rigidbody2D rb;

    private int nCheckpointsPassed;

    private void Start()
    {
        trackCheckpoints.OnPlayerCorrectCheckpoint += trackCheckpoints_OnCarCorrectCheckPoint;
        trackCheckpoints.OnPlayerWrongCheckpoint += trackCheckpoints_OnCarWrongCheckPoint;
        rb = this.GetComponent<Rigidbody2D>();
    }

    private void trackCheckpoints_OnCarWrongCheckPoint()
    {
        Debug.Log("NO");
        AddReward(-1f); 
    }

    private void trackCheckpoints_OnCarCorrectCheckPoint()
    {
        Debug.Log("YES");
        AddReward(1f);
        nCheckpointsPassed++;
        print("check"+nCheckpointsPassed);
    }

    public override void OnEpisodeBegin()
    {
        transform.position = initialPosition.position;
        transform.up = initialPosition.up;
        trackCheckpoints.ResetCheckpoint();
        rb.velocity = Vector2.zero;
        rb.rotation = 0;
        nCheckpointsPassed = 0;
    }

    public override void CollectObservations(VectorSensor sensor)
    {
        Vector3 checkpointForward = trackCheckpoints.GetNextCheckpoint().transform.up;
        float directionDot = Vector3.Dot(transform.up, checkpointForward);
        sensor.AddObservation(directionDot);
    }

    public override void OnActionReceived(ActionBuffers actions)
    {
        float verticalMove = 0f;
        float horizontalMove = 0f;

        switch (actions.DiscreteActions[0])
        {
            case 0:
                verticalMove = 1f;
                break;
            case 1:
                verticalMove = -1f;
                break;
            case 2:
                verticalMove = 0f;
                break;
        }
        switch (actions.DiscreteActions[1])
        {
            case 0:
                horizontalMove = 0f;
                break;
            case 1:
                horizontalMove = +1f;
                break;
            case 2:
                horizontalMove = -1f;
                break;
        }

        rb.AddForce(transform.up * verticalMove * Time.deltaTime * 75f);
        rb.rotation -= horizontalMove * 200f * Time.deltaTime;

        if (rb.velocity.magnitude > maxSpeed)
            rb.velocity = rb.velocity.normalized * maxSpeed;

    }

    private float maxSpeed = 1f;

    public override void Heuristic(in ActionBuffers actionsOut)
    {
        int verticalMove = 0;
        int horizontalMove = 0;

        if (Input.GetKey(KeyCode.W))
            verticalMove = 0;
        else if (Input.GetKey(KeyCode.S))
            verticalMove = 1;
        else
            verticalMove = 2;

        if (Input.GetKey(KeyCode.A))
            horizontalMove = 2;
        else if (Input.GetKey(KeyCode.D))
            horizontalMove = 1;
        else
            horizontalMove = 0;

        ActionSegment<int> discreteActions = actionsOut.DiscreteActions;
        discreteActions[0] = verticalMove;
        discreteActions[1] = horizontalMove;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Wall")
        {
            Debug.Log("F");
            AddReward(-0.5f);
            EndEpisode();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Goal" && nCheckpointsPassed > 60)
        {
            Debug.Log("GOAL");
            EndEpisode();
        }
    }
}
