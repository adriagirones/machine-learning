﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointSingle : MonoBehaviour 
{

    private TrackCheckpoints trackCheckpoints;
    private SpriteRenderer sprite;
    private BoxCollider2D box;

    private void Awake() 
    {
        sprite = GetComponent<SpriteRenderer>();
        box = GetComponent<BoxCollider2D>();
    }

    private void Start() 
    {
        Hide();
    }

    private void OnTriggerEnter2D(Collider2D collider) 
    {
        if (collider.gameObject.tag == "Player") 
            trackCheckpoints.CarThroughCheckpoint(this);
    }

    public void SetTrackCheckpoints(TrackCheckpoints trackCheckpoints) 
    {
        this.trackCheckpoints = trackCheckpoints;
    }

    public void Show() 
    {
        sprite.enabled = true;
    }

    public void Hide() 
    {
        sprite.enabled = false;
    }

    public void enableCollider()
    {
        box.enabled = true;
    }

    public void disableCollider()
    {
        box.enabled = false;
    }

}
