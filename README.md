# OBSERVACIONS

En primer lloc, el meu agent es guardarà la informació sobre el resultat del producte escalar del seu vector que mira cap endavant i del vector també up del següent checkpoint el qual ha d'anar. Amb aquesta informació l'agent tindrà informació sobre el vector de direcció el qual ha d'anar.

![](./GitImages/1.PNG)

L'agent tindrà un raycast el qual detectarà les parets i els checkpoint els quals després s'utilitzaran per sumar/restar recompenses.

![](./GitImages/5.PNG)


# DECISIONS

L'agent tindrà un Decision Requester en el qual cada frame rebrà una acció.

![](./GitImages/6.PNG)

# ACCIONS

Les accions del meu agent seran discretes. He decidit que tinguin dues branques: el movimient d'anar cap endavant i retrocedir, i el moviment de gir per canviar la rotació. Cada una d'aquestes branques tindrà 3 accions diferents (dues per la direcció i una per quedar-se quiet).

![](./GitImages/7.PNG)

En el OnAccionRecived es controlen aquestes accions i es produeixen les físiques del GameObject.

![](./GitImages/2.PNG)

# RECOMPENSES

Si el personatge col·lisiona amb el mur tindrà una petita recompensa negativa i es reiniciarà l'entrenament, així la próxima vegada intentarà evitar aquell mur. 

![](./GitImages/3.PNG)

Cada cop que el cotxe passa per un checkpoint salta un event. Si el cotxe passa per un checkpoint erroni, tindrà una recompensa negativa, en canvi si passa per el checkpoint correcte tindrà una recompensa positiva.

![](./GitImages/4.PNG)

En aquesta gràfica es veuen els primers entrenaments els quals el cotxe li costava passar de les primeres curves (es quedava en una recompensa de 10 aprox.)

![](./GitImages/8.PNG)

Però gràcies a unes millores en les observacions i ajusts en les recompenses, el cotxe aconsegueix fer una volta sencera al circuit.

![](./GitImages/9.PNG)

# VÍDEO

Exemple d'un entrenament:

![Vídeo](./GitImages/MLCar.mp4)
